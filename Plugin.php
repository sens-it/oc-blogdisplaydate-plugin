<?php namespace SENSIT\BlogDisplayDate;

use Backend;
use System\Classes\PluginBase;

/**
 * BlogDisplayDate Plugin Information File
 */
class Plugin extends PluginBase
{
    public $require = [
        'RainLab.Blog'
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'sensit.blogdisplaydate::lang.plugin.name',
            'description' => 'sensit.blogdisplaydate::lang.plugin.description',
            'author'      => 'SENS IT UG (haftungsbeschränkt)',
            'icon'        => 'icon-calendar-plus-o'
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        \RainLab\Blog\Models\Post::extend(function ($model) {
            $model->addDynamicMethod('hasDisplayDate', function () use ($model) {
                return true;
            });
            $model->addDynamicMethod('display_date', function () use ($model) {
                return $model->sensit_blogdisplaydate_displaydate ?? $model->published_at;
            });
        });
        
        \Event::listen('backend.form.extendFields', function ($widget) {
            
            // Only for the blog post controller
            if (! $widget->getController() instanceof \RainLab\Blog\Controllers\Posts) {
                return;
            }
            
            // Only for the blog post model
            if (! $widget->model instanceof \RainLab\Blog\Models\Post) {
                return;
            }
            
            if ($widget->getConfig('include', true)) {
                $widget->addSecondaryTabFields([
                        'sensit_blogdisplaydate_displaydate' => [
                                'label' => 'sensit.blogdisplaydate::lang.post.display_date',
                                'tab' => 'rainlab.blog::lang.post.tab_manage',
                                'type' => 'datepicker',
                                'mode' => 'date',
                                'span' => 'left',
                        ]
                ]);
            }
        });
    }
}
