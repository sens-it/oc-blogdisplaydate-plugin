<?php

namespace SENSIT\Blogdisplaydate\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class AddDisplayDateFieldToBlogTable extends Migration
{
    public function up()
    {
        Schema::table('rainlab_blog_posts', function ($table) {
            $table->date('sensit_blogdisplaydate_displaydate')->nullable();
        });
    }
    public function down()
    {
        Schema::table('rainlab_blog_posts', function ($table) {
            if (Schema::hasColumn('rainlab_blog_posts', 'sensit_blogdisplaydate_displaydate')) {
                $table->dropColumn('sensit_blogdisplaydate_displaydate');
            }
        });
    }
}
