<?php return [
    'plugin' => [
        'name' => 'Blog Display Date',
        'description' => 'Blog Display Date erweitert das RainLab.Blog Plugin mit einem zusätzlichen Feld für ein Datum, das sich mit dem Post anzeigen lässt.'
    ],
    'post' => [
        'display_date' => 'Anzeigedatum'
    ]
];
