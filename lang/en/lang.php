<?php return [
    'plugin' => [
        'name' => 'Blog Display Date',
        'description' => 'Blog Display Date extends the RainLab.Blog plugin with an additional field to specify a date to display with a blog post.'
    ],
    'post' => [
        'display_date' => 'Display Date'
    ]
];
