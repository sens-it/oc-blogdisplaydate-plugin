# Blog Display Date

Blog Display Date extends the RainLab.Blog plugin with an additional field to specify a date to display with a blog post.

![](docs/screenshot.png)


When the plugin is installed a new field **Display Date** will appear on the *Manage* tab of blog posts. The entered date is available as a property of the blog post when rendering it. This allows to specify a different date than the publish date to be displayed with the blog post.

The new field **Display Date** can be empty for blog posts. When accessing the `display_date` property it will default to the publish date and only return the display date when one was entered. The publish date makes for a good fallback value for displaying with the blog post.

The field values are stored in a new column added to the `rainlab_blog_posts` called `bdd_displaydate`.
